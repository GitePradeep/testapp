import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { AuthGuard } from '../../auth.guard';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  returnUrl: string;
  submitted = false;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authGuard: AuthGuard,
    private authService: AuthService,
    private fb: FormBuilder) { }

  ngOnInit(): void {
    this.initLoginForm();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/console/dashboard';
  }
  initLoginForm() {
    this.loginForm = this.fb.group({
      email: [!environment.production ? 'test@gmail.com' : '', Validators.required],
      password: [!environment.production ? '12345' : '', [Validators.required]]
  });
  }
  get f() { return this.loginForm.controls; }
  doLogin() {
    this.submitted = true;
    if (this.loginForm.invalid) {
     return;
    }
    this.authService.login(this.loginForm.value).subscribe(result => {
      if (result.error) {
        return;
      }
      if (!result.error) {
        this.authGuard.setCurrentUser(JSON.stringify(result.data));
        window.location.href = `${window.location.origin}/admin/dashboard`
        this.router.navigate([this.returnUrl]);
      }

    }, error => {
        console.log(error);
    });
  }

}
