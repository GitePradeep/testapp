import { Component, OnInit } from '@angular/core';
import {MoviesService} from '../../../services/movies.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  reportData: any;
  page: any;
  constructor(private movieList: MoviesService) { }

  ngOnInit(): void {
    this.getMoviiesList(1);
  }
  getMoviiesList(page:number) {
    this.movieList.list(page).subscribe(result => {
      if (result.error) {
        return;
      }
      if (!result.error) {
        this.reportData = result.results;
      }

    }, error => {
        console.log(error);
    });
  }
}
