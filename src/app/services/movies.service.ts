import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment'

@Injectable({
  providedIn: 'root'
})
export class MoviesService {
apiKey = '39add30cfc8c8572591450692af83c92';
  constructor(private http: HttpClient) { }
  list(page) {
    return this.http.get<any>(`${environment.websiteApiUrl}movie/popular?api_key=`+this.apiKey+'&page='+page); 
}
}
